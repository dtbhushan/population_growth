import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv('population-estimates.csv')
df1 = pd.read_csv('table-1.csv', usecols=['State'])
dict1 = {}
listASEAN = []
for i in range(len(df1)):
    listASEAN.append(df1['State'][i])
print(listASEAN)

for i in range(len(df)):
    if df['Region'][i] in listASEAN:
        if df['Year'][i] == 2014:
            dict1[df['Region'][i]] = df['Population'][i]

plt.bar(dict1.keys(), dict1.values())
plt.show()
