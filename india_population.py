
import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv('population-estimates.csv')
dict1 = {}
for i in range(len(df)):
    if df['Region'][i] == 'India':
        dict1[df['Year'][i]] = df['Population'][i]

print(dict1)
plt.bar(dict1.keys(), dict1.values())
plt.show()
