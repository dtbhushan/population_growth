
import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv('population-estimates.csv')
df1 = pd.read_csv('table-2.csv', usecols=['Country'])
listSAARC = []
for i in range(len(df1)):
    listSAARC.append(df1['Country'][i])

dict1 = {}
for i in range(1950, 2016):
    sum = 0
    for j in range(len(df)):
        if df['Year'][j] == i and df['Region'][j] in listSAARC:
            sum += df['Population'][j]

    dict1[i] = sum

plt.bar(dict1.keys(), dict1.values())
plt.show()
