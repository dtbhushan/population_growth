
import pandas as pd
from matplotlib import pyplot as plt
df = pd.read_csv('population-estimates.csv')
df1 = pd.read_csv('table-1.csv', usecols=['State'])
dict1 = {}
list_of_ASEAN = []
for i in range(len(df1)):
    list_of_ASEAN.append(df1['State'][i])
main_list = []
for i in range(2004, 2015):
    main_list.append([i])
k = 0
l2 = ['year']
for i in range(2004, 2015):
    for j in range(len(df)):
        if df['Year'][j] == i and df['Region'][j] in list_of_ASEAN:
            main_list[k].append(df['Population'][j])
            if k == 5:
                l2.append(df['Region'][j])
    k += 1
d = pd.DataFrame(main_list, columns=l2)
d.plot(x='year',
       kind='bar',
       stacked=False,
       title='Grouped Bar graph of ASEAN countries')

plt.show()
